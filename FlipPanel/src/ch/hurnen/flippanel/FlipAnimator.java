/*

The MIT License (MIT)

Copyright (c) 2014 Remo Marti

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

package ch.hurnen.flippanel;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.SwingUtilities;

/**
 * Runnable used to animate the {@link FlipPanel}.
 *
 */
class FlipAnimator implements Runnable {
		
		private final FlipPanel flipPanel;
		Timer timer= new Timer();

		public FlipAnimator(FlipPanel flipPanel) {
			this.flipPanel = flipPanel;
		}


		@Override
		public void run() {
			int start;
			int end;
			int inc;
			if (flipPanel.getValue() == 0) {
				start= 1;
				end= 100;
				inc= 1;
			} else if (flipPanel.getValue() == 1) {
				start= 99;
				end= 0;
				inc= -1;
			} else {
				return;
			}
			
			int i= start;
			int waitTime= 0;
			do {
				timer.schedule(new MyTask(flipPanel, i * 0.01), waitTime);
				i+= inc;
				waitTime+= 10;
			} while (i != end + inc);
	};
	
	private static class MyTask extends TimerTask {
		
		private double value;
		private FlipPanel flipPanel;

		public MyTask(FlipPanel flipPanel, double value) {
			this.flipPanel= flipPanel;
			this.value= value;
		}

		@Override
		public void run() {
			SwingUtilities.invokeLater(new Runnable(){

				@Override
				public void run() {
					flipPanel.setValue(value);
				}
				
			});
		}
		
	}
}