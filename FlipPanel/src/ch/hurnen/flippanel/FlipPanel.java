/*

The MIT License (MIT)

Copyright (c) 2014 Remo Marti

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

package ch.hurnen.flippanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;


/**
 * A flip panel is a special {@link JPanel} that has the ability to 'flip', i.e. to rotate the panel's
 * contents along the y-axis by 180 degree. As a result, the back side of the panel is visible.
 *
 */
public class FlipPanel extends JPanel {
	
	private static final String PREFERRED_SIZE = "preferredSize";

	private static final long serialVersionUID = 1L;
	
	// coordinates of the original rectangle
	private Point topLeft;
	private Point topRight;
	private Point bottomLeft;
	private Point bottomRight;
	
	private Rectangle originalRectangle;
	
	// coordinates of the transformed rectangle
	private Point2D.Double topLeftTransformed;
	private Point2D.Double topRightTransformed;
	private Point2D.Double bottomLeftTransformed;
	private Point2D.Double bottomRightTransformed;
	
	private Polygon transformedPolygon;

	// animation index. range: [0, 1]
	private double value;
	
	/* y distance of the two rear corners from the border.
	* Depends on the perspective
	*/
	private double rearY;

	// buffer containing the original (untransformed image)
	private BufferedImage original;
	
	// target buffer where transformed image is written to
	private BufferedImage transformed;

	// the component to be flipped
	private JComponent frontComponent;
	
	// the component displayed on the back
	private JComponent backComponent;

	private boolean gradientEnabled= true;
	
	private boolean useAntiAliasing= true;
	
	private boolean forkJoinEnabled= true;

	// image displayed in the background
	private Image backgroundImg;

	/*
	 * Influences the perspective of rotation.
	 * A value of 0 means: no perspective distortion.
	 * A value of 1 means: infinite perspective distortion.
	 */
	private double perspective= 0.5;
	
	private ForkJoinPool fjp= new ForkJoinPool();

	/**
	 * Constructor.
	 * Takes a {@link JComponent} for the panel's front side and another
	 * {@link JComponent} as the panel's back side that becomes visible only
	 * after a 'flip'.
	 * 
	 * @param frontComponent the component displayed on the front side
	 * @param backComponent the component displayed on the rear side
	 */
	public FlipPanel (JComponent frontComponent, final JComponent backComponent) {
		super(new BorderLayout());
		this.frontComponent= frontComponent;
				
		// listen to changes of frontComponent's preferred size
		addPreferredSizePropertyChangeListenerToFrontComponent(frontComponent);
		
		if (backComponent != null) {
			this.backComponent= buildBackComponent(backComponent);
		}
		initCornersAndPerspective(this.frontComponent.getSize());
		
		add(this.frontComponent, BorderLayout.CENTER);
		addResizeListener();
	}
	
	/**
	 * Constructor.
	 * Takes a {@link JComponent} for the panel's front side.
	 * The back side can be set later with {@link #setBackComponent(JComponent)}, but
	 * must be set before a 'flip' takes place.
	 * 
	 * @param frontComponent the component displayed on the front side
	 */
	public FlipPanel (JComponent frontComponent) {
		this(frontComponent, null);
	}

	private JScrollPane buildBackComponent(final JComponent backComponent) {
		
		/* this panel has the size of the front component and contains
		 * the backComponent top left aligned.
		 */
		JPanel backComponentFillContainer= new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		backComponentFillContainer.add(backComponent);
		
		// back component is wrapped by a scroll pane so its size never exceeds the front component.
		JScrollPane backComponentScrollPane = new JScrollPane(backComponentFillContainer);
		backComponentScrollPane.setBorder(BorderFactory.createEmptyBorder());
		
		addPreferredSizePropertyChangeListenerToBackComponent(backComponent);
		
		return backComponentScrollPane;
	}

	private void addResizeListener() {
		addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// empty
			}
			
			@Override
			public synchronized void componentResized(ComponentEvent e) {
				initCornersAndPerspective(getSize());
				
				// recalculate image buffers on resize
				if (!(value == 0 || value == 1)) {
					original= calculateOriginalBuffer();
					transformed= new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_INT_ARGB);
				}				
				
				setValue(value);
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// empty
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// empty
			}
		});
	}

	private void addPreferredSizePropertyChangeListenerToBackComponent(final JComponent backComponent) {
		backComponent.addPropertyChangeListener(PREFERRED_SIZE, new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				backComponent.revalidate();
			}
		});
	}

	private void addPreferredSizePropertyChangeListenerToFrontComponent(final JComponent frontComponent) {
		frontComponent.addPropertyChangeListener(PREFERRED_SIZE, new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				FlipPanel.this.setPreferredSize((Dimension)evt.getNewValue());
				FlipPanel.this.revalidate();
			}
		});
	}

	private void initCornersAndPerspective(Dimension dimension) {
		this.topLeft = new Point(0, 0);
		this.topRight = new Point((int)dimension.getWidth() - 1, 0);
		this.bottomLeft = new Point(0, (int)dimension.getHeight() - 1);
		this.bottomRight = new Point((int)dimension.getWidth() - 1, (int)dimension.getHeight() - 1);
		
		this.topLeftTransformed = new Point2D.Double(topLeft.getX(), topLeft.getY());
		this.topRightTransformed = new Point2D.Double(topRight.getX(), topRight.getY());
		this.bottomLeftTransformed = new Point2D.Double(bottomLeft.getX(), bottomLeft.getY());
		this.bottomRightTransformed = new Point2D.Double(bottomRight.getX(), bottomRight.getY());
		
		originalRectangle= calculateOriginalRectangle();
		
		this.rearY= calculateRearY();
	}

	private double calculateRearY() {
		return perspective * 0.5 * getDistanceY();
	}
	
	
	/**
	 * Renders invisible component
	 * 
	 * @see http://stackoverflow.com/questions/7369814/why-does-the-jtable-header-not-appear-in-the-image?lq=1
	 * @param component
	 * @return
	 */
	private BufferedImage captureInvisibleComponent (JComponent component) {
		JPanel fakePanel = new JPanel(new BorderLayout());
        fakePanel.add(component, BorderLayout.CENTER);

        fakePanel.addNotify();
        fakePanel.setSize(this.getSize());

        // forces recursive doLayout of children
        fakePanel.validate();
		return captureVisibleComponent(component);
	}
	
	private BufferedImage captureVisibleComponent(JComponent c) {
		c.setSize(this.getSize());
	    BufferedImage img = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g = img.createGraphics();
	    c.paintAll(g);
	    g.dispose();
	    return img;
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (original == null) {
			return;
		}
		super.paintComponent(g);
		Graphics2D g2= (Graphics2D) g;
		
		g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_SPEED);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		if (backgroundImg != null) {
			paintTiledBackground(g2);
		}
		
		// smooth gradient border
		paintInnerBorder(g2);
		
		calculateTransformedImage(original, transformed);
		g2.drawImage(transformed, 0, 0, null);
		
		if (gradientEnabled) {
			paintGradient(g2);
		}
	}

	/**
	 * Paints the background image.
	 * 
	 * @param g2
	 */
	protected void paintTiledBackground(Graphics2D g2) {
		Rectangle clip = g2.getClipBounds();

		int width= backgroundImg.getHeight(this);
		int height= backgroundImg.getWidth(this);

		for(int x = clip.x; x < (clip.x + clip.width) ; x += width) {
			for(int y = clip.y; y < (clip.y + clip.height) ; y += height) {
				g2.drawImage(backgroundImg,x,y,this);
			}
		}
	}

	/**
	 * Paints the border 'behind' the flip panel.
	 * @param g2
	 */
	protected void paintInnerBorder(Graphics2D g2) {
		int size= 10;
		
		Point innerTopLeft= new Point((int)topLeft.getX() + size, (int)topLeft.getY() + size);
		Point innerTopRight= new Point((int)topRight.getX() - 0, (int)topRight.getY() + size);
		Point innerBottomLeft= new Point((int)bottomLeft.getX() + size, (int)bottomLeft.getY() - 0);
		
		GradientPaint gp = new GradientPaint(
				(float)topLeft.getX(), 
				(float)topLeft.getY(), 
				new Color(0, 0, 0, 120), 
				(float)topLeft.getX(), 
				(float)topLeft.getY() + size,
				new Color(0, 0, 0, 0)
				);
		g2.setPaint(gp);
		
		// top
		g2.fillPolygon(
				new int[]{(int)topLeft.getX(), (int)topRight.getX(), (int)innerTopRight.getX(), (int)innerTopLeft.getX()}, 
				new int[]{(int)topLeft.getY(), (int)topRight.getY(), (int)innerTopRight.getY(), (int)innerTopLeft.getY()}, 4);
		
		gp = new GradientPaint(
				(float)topLeft.getX(), 
				(float)topLeft.getY(), 
				new Color(0, 0, 0, 120), 
				(float)topLeft.getX() + size, 
				(float)topLeft.getY(),
				new Color(0, 0, 0, 0)
				);
		g2.setPaint(gp);
		
		// left
		g2.fillPolygon(
				new int[]{(int)topLeft.getX(), (int)innerTopLeft.getX(), (int)innerBottomLeft.getX(), (int)bottomLeft.getX()}, 
				new int[]{(int)topLeft.getY(), (int)innerTopLeft.getY(), (int)innerBottomLeft.getY(), (int)bottomLeft.getY()}, 4);
	}

	/**
	 * Calculates the transformed image from the original image.
	 * 
	 * @param original
	 * @param transformed
	 */
	private void calculateTransformedImage(BufferedImage original, BufferedImage transformed) {
		// clear buffer
		Graphics2D graphics = (Graphics2D) transformed.getGraphics();
		graphics.setBackground(new Color(0, 0, 0, 0));
        graphics.clearRect(0,0, transformed.getWidth(), transformed.getHeight());
        
        if (forkJoinEnabled) {
        	fjp.invoke(new ForkJoinRenderer(0, original.getWidth(), 0, original.getHeight()));
        } else {
        	new FlipPanelRenderer(0, original.getWidth(), 0, original.getHeight()).run();
        }
	}


	/**
	 * Paints the perspective gradient over the transformed image.
	 * 
	 * @param g2
	 */
	protected void paintGradient(Graphics2D g2) {
		double backendTransparencyValue;
		if (value <= 0.5) {
			backendTransparencyValue= 2 * value;
		} else {
			backendTransparencyValue= (1 - value) * 2;
		}
		GradientPaint gp = new GradientPaint(
				(int)topLeftTransformed.getX(), 
				(int)topLeft.getY(), 
				// rear color / transparency
				new Color(0, 0, 0, (int)Math.floor(backendTransparencyValue * 255 * 0.75)), 
				(int)topRightTransformed.getX(), 
				(int)topRight.getY(),
				// front color/transparency
				new Color(0, 0, 0, 0)
				);
		g2.setPaint(gp);
		g2.fillPolygon(transformedPolygon);
	}

	private Polygon calculateTransformedPolygon() {
		Polygon transformedPolygon= new Polygon();
		transformedPolygon.addPoint((int)Math.round(topLeftTransformed.getX()), (int)Math.round(topLeftTransformed.getY()));
		transformedPolygon.addPoint((int)Math.round(topRightTransformed.getX()), (int)Math.round(topRightTransformed.getY()));
		transformedPolygon.addPoint((int)Math.round(bottomRightTransformed.getX()), (int)Math.round(bottomRightTransformed.getY()));
		transformedPolygon.addPoint((int)Math.round(bottomLeftTransformed.getX()), (int)Math.round(bottomLeftTransformed.getY()));
		return transformedPolygon;
	}
	
	private Rectangle calculateOriginalRectangle() {
		Rectangle rect= new Rectangle();
		rect.add((int)topLeft.getX(), (int)topLeft.getY());
		rect.add((int)topRight.getX(), (int)topRight.getY());
		rect.add((int)bottomRight.getX(), (int)bottomRight.getY());
		rect.add((int)bottomLeft.getX(), (int)bottomLeft.getY());
		return rect;
	}

	@Override
	public Dimension getPreferredSize() {
		return frontComponent.getPreferredSize();
	}
	
	
	@Override
	public Dimension getMaximumSize() {
		return frontComponent.getMaximumSize();
	}

	@Override
	public Dimension getMinimumSize() {
		return frontComponent.getMinimumSize();
	}

	private int getDistanceX() {
 		return (int) Math.abs(topLeft.getX() - topRight.getX());
	}
	
	private int getDistanceY() {
		return (int) Math.abs(topLeft.getY() - bottomLeft.getY());
	}
	
	/**
	 * Calculates the current rear y distance from the border.
	 * 
	 * @return
	 */
	private double getRearYDistanceFromBorder() {
		if (value <= 0.5) {
			return value * rearY * 2;
		} else {
			return (1-value) * rearY * 2;
		}
	}
	
	/**
	 * Returns the current animation value.
	 * @return the current animation value in the range [0, 1].
	 */
	public double getValue () {
		return value;
	}

	/**
	 * Sets the current animation value. Range: [0, 1].
	 * A value of 0 means: front component visible.
	 * A value of 1 means: full flip, back component visible.
	 * @param value animation value
	 */
	public void setValue(double value) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException("value in range [0, 1] expected. Got: " + value);
		}
		if (value >= 0.5 && backComponent == null) {
			throw new IllegalArgumentException("No back component available.");
		}
		double oldValue= this.value;
		this.value = value;
		
		// displayed component changes between front and back
		if (oldValue <= 0.5 && value > 0.5 || oldValue >= 0.5 && value < 0.5) {
			original= null;
		}
		
		if (value == 0) {
			add(frontComponent, BorderLayout.CENTER);
			original= null;
			repaint();
			return;
		} else if (value == 1) {
			add(backComponent, BorderLayout.CENTER);
			original= null;
			repaint();
			return;
		} else {
			if (original == null) {
				original= calculateOriginalBuffer();
				transformed= new BufferedImage(original.getWidth(), original.getHeight(), BufferedImage.TYPE_INT_ARGB);
			}
			calculateCornerTransformation();
			transformedPolygon= calculateTransformedPolygon();
			remove(frontComponent);
			if (backComponent != null) {
				remove(backComponent);				
			}
		}
	}

	/**
	 * (Re-) calculates the original image buffer from the
	 * component currently visible.
	 * 
	 * @return
	 */
	private BufferedImage calculateOriginalBuffer() {
		JComponent component;
		
		boolean doMirror= false;
		if (value <= 0.5) {
			component = frontComponent;
		} else {
			component= backComponent;
			doMirror= true;
		}
		BufferedImage buffer;
		if (!component.isDisplayable()) {
			buffer= captureInvisibleComponent(component);
		} else {
			buffer= captureVisibleComponent(component);
		}
		if (doMirror) {
			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
		    tx.translate(-buffer.getWidth(), 0);
		    AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BICUBIC);
		    buffer = op.filter(buffer, null);
		}
		
		return buffer;
	}

	private void calculateCornerTransformation() {
		topRightTransformed.setLocation(topRight.getX() - value * getDistanceX(), topRightTransformed.getY());
		bottomRightTransformed.setLocation(bottomRight.getX() - value * getDistanceX(), bottomRightTransformed.getY());
		topLeftTransformed.setLocation(topLeft.getX() + value * getDistanceX(), topLeft.getY() + getRearYDistanceFromBorder());
		bottomLeftTransformed.setLocation(bottomLeft.getX() + value * getDistanceX(), bottomLeft.getY() - getRearYDistanceFromBorder());

		repaint();
	}
	
	/**
	 * Calculates the coordinates of the original image for the given
	 * transformed coordinates.
	 * 
	 * @param transformedCoordinates
	 * @return
	 */
	private Point2D getOriginalCoordinates (Point transformedCoordinates) {
		double origX= getDistanceX() * ((transformedCoordinates.getX() - topLeftTransformed.getX()) / (topRightTransformed.getX() - topLeftTransformed.getX()));
		double ratioX= origX / getDistanceX();
		
		double topY= topLeftTransformed.getY() - ratioX * (-topRightTransformed.getY() + topLeftTransformed.getY());
		double bottomY= bottomLeftTransformed.getY() + ratioX * (bottomRightTransformed.getY() - bottomLeftTransformed.getY());
		double origY= getDistanceY() * ((transformedCoordinates.getY() - topY) / (bottomY - topY));
		
		return new Point2D.Double(origX, origY);
	}

	/**
	 * Enable or disable the gradient paint for a better illusion of 3D rotation.
	 * 
	 * @param enableGradient
	 */
	public void setGradientEnabled(boolean enableGradient) {
		this.gradientEnabled= enableGradient;
		repaint();
	}
	
	/**
	 * Returns whether the gradient paint is enabled.
	 * @return whether the gradient paint is enabled.
	 */
	public boolean isGradientEnabled () {
		return this.gradientEnabled;
	}

	/**
	 * Sets a value for the perspective distortion in the range [0, 1].
	 * 
	 * @param perspective
	 */
	public void setPerspective(double perspective) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException("value in range [0, 1] expected.");
		}
		this.perspective= perspective;
		this.rearY= calculateRearY();
		calculateCornerTransformation();
		transformedPolygon= calculateTransformedPolygon();
	}
	
	/**
	 * Set background image. The image will be tiled to
	 * fully cover the background behind the flip panel.
	 * 
	 * @param background
	 */
	public void setBackgroundImage (Image background) {
		this.backgroundImg= background;
	}

	/**
	 * Returns whether anti-alias is enabled.
	 * @return whether anti-alias is enabled
	 */
	public boolean isAntiAliasingEnabled() {
		return useAntiAliasing;
	}

	/**
	 * Turns anti-aliasing on and of. It is calculated in software
	 * and therefore an expensive operation that results in lower
	 * frame rates.
	 * 
	 * @param enableAntiAliasing
	 */
	public void setAntiAliasingEnabled(boolean enableAntiAliasing) {
		this.useAntiAliasing= enableAntiAliasing;
		repaint();
	}
	
	/**
	 * Renders all pixels of the transformed image buffer within the given bounds.
	 * 
	 */
	private class FlipPanelRenderer implements Runnable {
		private int fromX;
		private int toX;
		private int fromY;
		private int toY;

		public FlipPanelRenderer(int fromX, int toX, int fromY, int toY) {
			if (fromX > toX || fromY > toY) {
				throw new IllegalArgumentException("fromX: " + fromX + " > toX: " + toX + " || fromY: " + fromY + " > toY: " + toY);
			}
			this.fromX= fromX;
			this.toX= toX;
			this.fromY= fromY;
			this.toY= toY;
		}

		@Override
		public void run() {
			for (int x= fromX; x < toX; x++) {
				// optimization: don't calculate pixels outside transformed polygon
				if (value <= 0.5 && (x < topLeftTransformed.getX() || x > topRightTransformed.getX()) ||
					value > 0.5 && (x < topRightTransformed.getX() || x > topLeftTransformed.getX())) {
					continue;
				}
				
				for (int y= fromY; y < toY; y++) {
					if (transformedPolygon.contains(x, y)) {
						Point2D originalCoordinates= getOriginalCoordinates(new Point(x,y));
						
						if (useAntiAliasing) {
							int floorX= (int)originalCoordinates.getX();
							int floorY= (int)originalCoordinates.getY();
							
							// fractional part of coordinates
							double nx= originalCoordinates.getX() - floorX;
							double ny= originalCoordinates.getY() - floorY;
							
							int sourceX;
							int sourceY;
							int origColAtSource;
							
							double alpha1= 0;
							double alpha2= 0;
							double alpha3= 0;
							double alpha4= 0;
							
							// top left
							alpha1= ((1 - nx) * (1 - ny));
							sourceX= floorX;
							sourceY= floorY;
							int a1= 0;
							int r1= 0;
							int g1= 0;
							int b1= 0;
							
							if (originalRectangle.contains(sourceX, sourceY)) {
								origColAtSource= original.getRGB(sourceX, sourceY);
								
								a1= (origColAtSource >> 24) & 0xFF;
								r1= (origColAtSource >> 16) & 0xFF;
								g1= (origColAtSource >> 8) & 0xFF;
								b1=  (origColAtSource) & 0xFF;
							}
			
							
							// top right
							int a2= 0;
							int r2= 0;
							int g2= 0;
							int b2= 0;
							if (nx != 0) {
								alpha2= (nx * (1 - ny));
								sourceX= floorX + 1;
								sourceY= floorY;
								
								if (originalRectangle.contains(sourceX, sourceY)) {
									origColAtSource= original.getRGB(sourceX, sourceY);
									
									a2= (origColAtSource >> 24) & 0xFF;
									r2= (origColAtSource >> 16) & 0xFF;
									g2= (origColAtSource >> 8) & 0xFF;
									b2=  (origColAtSource) & 0xFF;
								}
							}
							
							// bottom left
							int a3= 0;
							int r3= 0;
							int g3= 0;
							int b3= 0;
							if (ny != 0) {
								alpha3= ((1 - nx) * ny);
								sourceX= floorX;
								sourceY= floorY + 1;
								
								if (originalRectangle.contains(sourceX, sourceY)) {
									origColAtSource= original.getRGB(sourceX, sourceY);
									
									a3= (origColAtSource >> 24) & 0xFF;
									r3= (origColAtSource >> 16) & 0xFF;
									g3= (origColAtSource >> 8) & 0xFF;
									b3=  (origColAtSource) & 0xFF;
								}
							}
							
							// bottom right
							int a4= 0;
							int r4= 0;
							int g4= 0;
							int b4= 0;
							if (ny != 0 && nx != 0) {
								alpha4= (nx * ny);
								sourceX= floorX + 1;
								sourceY= floorY + 1;
								
								if (originalRectangle.contains(sourceX, sourceY)) {
									origColAtSource= original.getRGB(sourceX, sourceY);
									
									a4= (origColAtSource >> 24) & 0xFF;
									r4= (origColAtSource >> 16) & 0xFF;
									g4= (origColAtSource >> 8) & 0xFF;
									b4=  (origColAtSource) & 0xFF;
								}
							}
							
							int aCombined= (int)(alpha1 * a1 + alpha2 * a2 + alpha3 * a3 + alpha4 * a4);
							int rCombined= (int)(alpha1 * r1 + alpha2 * r2 + alpha3 * r3 + alpha4 * r4);
							int gCombined= (int)(alpha1 * g1 + alpha2 * g2 + alpha3 * g3 + alpha4 * g4);
							int bCombined= (int)(alpha1 * b1 + alpha2 * b2 + alpha3 * b3 + alpha4 * b4);
							
							transformed.setRGB(x, y, ((aCombined & 0x0ff) << 24) | ((rCombined & 0x0ff) << 16) | ((gCombined & 0x0ff) << 8) | (bCombined & 0x0ff));
							
						} else {
							int sourceX= (int)Math.round(originalCoordinates.getX());
							int sourceY= (int)Math.round(originalCoordinates.getY());
							if (originalRectangle.contains(sourceX, sourceY)) {
								transformed.setRGB(x, y, original.getRGB(sourceX, sourceY));
							}
						}
						
					}
				}
			}
		}
		
	}
	
	/**
	 * Executes multiple instances of the {@link FlipPanelRenderer} in parallel to improve
	 * speed. Uses Java's fork join mechanism. 
	 *
	 */
	private class ForkJoinRenderer extends RecursiveAction {

		private static final long serialVersionUID = 1L;
		
		private int fromX;
		private int toX;
		private int fromY;
		private int toY;
		private static final int THRESHOLD= 5_000;

		public ForkJoinRenderer(int fromX, int toX, int fromY, int toY) {
			if (fromX > toX || fromY > toY) {
				throw new IllegalArgumentException("fromX: " + fromX + " > toX: " + toX + " || fromY: " + fromY + " > toY: " + toY);
			}
			this.fromX= fromX;
			this.toX= toX;
			this.fromY= fromY;
			this.toY= toY;
		}

		@Override
		protected void compute() {
			int nrOfPixels= (toX - fromX) * (toY - fromY);
			
			if (nrOfPixels > THRESHOLD) {
				int halfY= (int)Math.floor((toY - fromY) / 2) + fromY;
				invokeAll(
						new ForkJoinRenderer(fromX, toX, fromY, halfY),
						new ForkJoinRenderer(fromX, toX, halfY, toY)
						);
			} else {
				new FlipPanelRenderer(fromX, toX, fromY, toY).run();
			}
		}
	}

	/**
	 * Returns whether the fork/join algorithm has been enabled to improve
	 * rendering speed. This is especially recommended on multi-core machines.
	 * @return whether fork/join is enabled.
	 */
	public boolean isForkJoinEnabled() {
		return forkJoinEnabled;
	}

	/**
	 * Enable/disable fork/join algorithm to improve rendering speed on
	 * multi-core machines.
	 * @param enableForkJoin enable or disable fork/join
	 */
	public void setForkJoinEnabled(boolean enableForkJoin) {
		this.forkJoinEnabled= enableForkJoin;
	}
	
	/**
	 * Flips the panel between front and back side. The flip animation is only
	 * performed, if the current animation value {@link #getValue()} is either
	 * 0 or 1. A call of {@link #flip()} does nothing otherwise.
	 */
	public void flip () {
		SwingUtilities.invokeLater(new FlipAnimator(this));
	}
	
	/**
	 * Sets or replaces the back component.
	 * 
	 * @param backComponent the component visible on the back side of
	 * the flip panel.
	 */
	public void setBackComponent (JComponent backComponent) {
		if (this.backComponent != null) {
			// clean-up existing back component
			remove(this.backComponent);
		}
		this.backComponent= buildBackComponent(backComponent);
	}
	
}
