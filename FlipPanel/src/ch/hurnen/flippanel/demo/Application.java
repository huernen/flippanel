/*

The MIT License (MIT)

Copyright (c) 2014 Remo Marti

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

package ch.hurnen.flippanel.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ch.hurnen.flippanel.FlipPanel;

/**
 * Example application demonstrating the {@link FlipPanel}s functionality.
 */
public class Application extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private AbstractAction showGradientAction;
	private FlipPanel flipPanel;
	private AbstractAction useAntiAliasingAction;
	private AbstractAction enableForkJoinAction;
	private AbstractAction animationAction;
	private JSlider animationSlider;

	public Application() {
		buildActions();
		buildUI();
	}
	
	public static void main(String[] args) {
		Application app= new Application();
		app.setVisible(true);
	}
	
	private void buildUI() {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(new JLabel("FlipPanel Demo"), BorderLayout.NORTH);
		
		final JScrollPane frontComponent = createFrontComponent();
	    final JPanel backComponent = createBackComponent(frontComponent);
		
	    // create actual flip panel
	    flipPanel= new FlipPanel(frontComponent, backComponent);
	    flipPanel.setBackgroundImage(new ImageIcon(Application.class.getClassLoader().getResource("demo/fabric.png")).getImage());
		
		JPanel controls = createControls(frontComponent, backComponent);
	    
	    JPanel centerPanel= new JPanel(new BorderLayout());
	    centerPanel.setBorder(BorderFactory.createTitledBorder("Display"));
	    
	    centerPanel.add(flipPanel, BorderLayout.CENTER);
		add(centerPanel, BorderLayout.CENTER);
		add(controls, BorderLayout.SOUTH);
		
		pack();
	}

	private JPanel createControls(final JScrollPane frontComponent, final JPanel backComponent) {
		JPanel settings= new JPanel();
		settings.setBorder(BorderFactory.createTitledBorder("Settings"));
		BoxLayout boxLayout= new BoxLayout(settings, BoxLayout.Y_AXIS);
		settings.setLayout(boxLayout);
		
		animationSlider = createAnimationSlider();
		animationSlider.setAlignmentX(0);
		animationSlider.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				flipPanel.setValue(((JSlider)e.getSource()).getValue() / 100.0);
			}});
		settings.add(createLabelAndControl("Animation Step: ", animationSlider));
		
		JSlider perspectiveSlider= createPerspectiveSlider();
		perspectiveSlider.setAlignmentX(0);
		perspectiveSlider.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				flipPanel.setPerspective(((JSlider)e.getSource()).getValue() / 100.0);
			}});
		settings.add(createLabelAndControl("Perspective Distortion: ", perspectiveSlider));
		
		JCheckBox checkBox= new JCheckBox(showGradientAction);
		checkBox.setSelected(true);
		settings.add(createControl(checkBox, BorderLayout.WEST));
		JCheckBox antiAliasing= new JCheckBox(useAntiAliasingAction);
		antiAliasing.setSelected(true);
		JCheckBox forkJoin= new JCheckBox(enableForkJoinAction);
		forkJoin.setSelected(true);
		settings.add(createControl(antiAliasing, BorderLayout.WEST));
		settings.add(createControl(forkJoin, BorderLayout.WEST));
	    
	    settings.add(createLabelAndControl("Front Component Width: ", createFrontWidthSlider(frontComponent)));
	    settings.add(createLabelAndControl("Front Component Height: ", createFrontHeightSlider(frontComponent)));
	    settings.add(createLabelAndControl("Back Component Size: ", createBackSizeSlider(backComponent)));
	    settings.add(createControl(new JButton(animationAction), BorderLayout.EAST));
		return settings;
	}

	private JSlider createBackSizeSlider(final JPanel backComponent) {
		final JSlider backComponentSlider= new JSlider(10, 1000, 50);
	    backComponentSlider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				backComponent.setPreferredSize(new Dimension(backComponentSlider.getValue(), backComponentSlider.getValue()));
			}
		});
		return backComponentSlider;
	}

	private JSlider createFrontHeightSlider(final JScrollPane frontComponent) {
		final JSlider heightSlider= new JSlider(1, 2000, 300);
	    heightSlider.addChangeListener(new ChangeListener() {
	    	
	    	@Override
	    	public void stateChanged(ChangeEvent e) {
	    		frontComponent.setPreferredSize(new Dimension((int)frontComponent.getPreferredSize().getWidth(), heightSlider.getValue()));
	    	}
	    });
		return heightSlider;
	}

	private JSlider createFrontWidthSlider(final JScrollPane frontComponent) {
		final JSlider widthSlider= new JSlider(1, 2000, 400);
	    widthSlider.addChangeListener(new ChangeListener() {
	    	
	    	@Override
	    	public void stateChanged(ChangeEvent e) {
	    		frontComponent.setPreferredSize(new Dimension(widthSlider.getValue(), (int)frontComponent.getPreferredSize().getHeight()));
	    	}
	    });
		return widthSlider;
	}

	private JScrollPane createFrontComponent() {
		Object rowData[][] = {
				{ "Row1-Column1", "Row1-Column2", "Row1-Column3" },
		        { "Row2-Column1", "Row2-Column2", "Row2-Column3" }, 
		        { "Row3-Column1", "Row3-Column2", "Row3-Column3" },
		        { "Row4-Column1", "Row4-Column2", "Row4-Column3" },
		        { "Row5-Column1", "Row5-Column2", "Row5-Column3" },
		        { "Row6-Column1", "Row6-Column2", "Row6-Column3" },
		        { "Row7-Column1", "Row7-Column2", "Row7-Column3" },
		        { "Row8-Column1", "Row8-Column2", "Row8-Column3" },
				};
	    Object columnNames[] = { "Column One", "Column Two", "Column Three" };
	    JTable table = new JTable(rowData, columnNames);
	    table.getSelectionModel().addSelectionInterval(3, 6);
	    final JScrollPane frontComponent = new JScrollPane(table);
	    frontComponent.setPreferredSize(new Dimension(400, 300));
		return frontComponent;
	}

	private Component createLabelAndControl(String label, Component component) {
		JPanel panel= new JPanel(new BorderLayout());
		panel.add(new JLabel(label), BorderLayout.WEST);
		panel.add(component, BorderLayout.EAST);
		return panel;
	}
	
	private Component createControl(Component component, String layoutConstraint) {
		JPanel panel= new JPanel(new BorderLayout());
		panel.add(component, layoutConstraint);
		return panel;
	}

	private JPanel createBackComponent(final JComponent frontComponent) {
		JPanel backComponent= new JPanel();
		backComponent.setBackground(Color.red);
		backComponent.setPreferredSize(new Dimension(100, 100));
		backComponent.setBorder(BorderFactory.createLineBorder(Color.black));
	    
		return backComponent;
	}
	
	
	private JSlider createAnimationSlider () {
		JSlider slider= new JSlider(0, 100);
		slider.setValue(0);
		
		return slider;
	}
	
	private JSlider createPerspectiveSlider () {
		JSlider slider= new JSlider(0, 100);
		slider.setValue(50);
		
		return slider;
	}
	
	
	private void buildActions () {
		showGradientAction= new AbstractAction("Show perspective gradient"){

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				flipPanel.setGradientEnabled(!flipPanel.isGradientEnabled());
			}
			
		};
		
		useAntiAliasingAction= new AbstractAction("Enable Anti-Aliasing"){
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				flipPanel.setAntiAliasingEnabled(!flipPanel.isAntiAliasingEnabled());
			}
			
		};
		
		enableForkJoinAction= new AbstractAction("Enable Fork/Join"){
			
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				flipPanel.setForkJoinEnabled(!flipPanel.isForkJoinEnabled());
			}
			
		};
		
		animationAction= new AbstractAction("Flip!"){

			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				flipPanel.flip();
			}
			
		};
	}
}
